import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';

declare var google: any;

@Component({
  selector: 'app-direct-forms',
  templateUrl: './direct-forms.component.html',
  styleUrls: ['./direct-forms.component.css']
})
export class DirectFormsComponent implements OnInit {
  directForm: FormGroup;

  constructor( private mapLoader: MapsAPILoader) { }

  ngOnInit() {
    this.initForm();
  }

  onSubmit() {
    
  }

  private initForm(){
    let address = '';
    let city = '';
    let state = '';
    let zip = '';

    this.directForm = new FormGroup({
      'address': new FormControl(address),
      'city': new FormControl(city),
      'state': new FormControl(state),
      'zip': new FormControl(zip)
    })
  }

}
