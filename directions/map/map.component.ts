import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor() { }

  LatLng: {lat: number, lng: number}
  
  ngOnInit() {
  }

  zoom: number = 17;
  lat: number = 32.508277;
  lng: number = -84.971336;
  origin = {lat: 32.508848, lng: -84.971565}
  destination = {lat: 32.508277, lng: -84.971336}
}
