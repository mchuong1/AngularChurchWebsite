import {NgModule} from '@angular/core';
import { Routes, RouterModule, PreloadAllModules} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DirectionsComponent } from './directions/directions.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'directions', component: DirectionsComponent}
];

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {

}