import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { MDBBootstrapModule, WavesModule, NavbarModule } from 'angular-bootstrap-md';
import { CarouselComponent } from './home/carousel/carousel.component';
import { DirectionsComponent } from './directions/directions.component';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { MapComponent } from './directions/map/map.component';
import { DirectFormsComponent } from './directions/direct-forms/direct-forms.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InformationComponent } from './home/information/information.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    CarouselComponent,
    DirectionsComponent,
    MapComponent,
    DirectFormsComponent,
    InformationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    WavesModule,
    NavbarModule,
    AgmCoreModule.forRoot({
      apiKey: '...'
    }),
    ReactiveFormsModule,
    AgmDirectionModule,
    HttpClientModule
  ],
  providers: [GoogleMapsAPIWrapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
